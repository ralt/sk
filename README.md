# sk

- `daemon/`: the daemon managing the FUSE filesystem.
- `cli/`: the CLI used to manage files.
- `popup/`: the GUI application managing the popup when opening a
  file.
