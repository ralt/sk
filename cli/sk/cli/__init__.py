from contextlib import contextmanager
import errno
import json
import logging
import socket
import struct

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)


@contextmanager
def open_client():
    client = Client()
    try:
        yield client
    finally:
        client.close()


class CommandException(Exception):
    pass


class Client(object):

    def __init__(self):
        self._socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        try:
            self._socket.connect("/run/sk.cli.sock")
        except socket.error as e:
            if e.errno == errno.ECONNREFUSED:
                logger.error("Server is not started.")
                return
            raise

    def close(self):
        self._socket.close()

    def __getattr__(self, command_name):
        def _method(*args):
            command = {
                "name": command_name,
                "arguments": args,
            }
            command_as_string = json.dumps(command)
            self._socket.send(struct.pack(">I", len(command_as_string)))
            self._socket.send(command_as_string.encode("utf-8"))
            exit_code = self._socket.recv(1)[0]
            if exit_code != 0:
                raise CommandException(
                    "%s failed with exit code %s" % (command_name, exit_code)
                )
        return _method
