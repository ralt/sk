from sk.cli.commands.device import device
from sk.cli.commands.file import file


def add_commands(group):
    group.add_command(device)
    group.add_command(file)
