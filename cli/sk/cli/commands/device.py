import click

from .. import open_client


@click.group()
def device():
    pass


@click.command()
@click.argument("device")
@click.argument("name")
def add(device, name):
    with open_client() as client:
        client.device_add(device, name)

device.add_command(add)
