import click

from .. import open_client


@click.group()
def file():
    pass


@click.command()
@click.argument("device_name")
@click.argument("device_path")
@click.argument("local_path")
def add(device_name, device_path, local_path):
    with open_client() as client:
        client.file_add(device_name, device_path, local_path)

file.add_command(add)
