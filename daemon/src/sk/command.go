package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"os/exec"
	"strings"
)

const (
	SUCCESS = iota
	UNKNOWN_COMMAND
	DB_OPEN_ERROR
	BEGIN_TRANSACTION_ERROR
	PREPARE_ERROR
	QUERY_ERROR
	SCAN_ERROR
	INSERT_ERROR
	DEVICE_UUID_ERROR
	DEVICE_EXISTS
)

const DB = "/var/lib/sk/db"

type command struct {
	Name      string
	Arguments []string
}

func openDatabase() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", DB)
	if err != nil {
		return nil, err
	}

	_, err = db.Exec("create table if not exists devices (name varchar(255), fs_uuid blob)")
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (c *command) unknownCommand() byte {
	log.Printf("Unknown command: %s", c.Name)
	return UNKNOWN_COMMAND
}

func (c *command) deviceAdd() byte {
	device := c.Arguments[0]
	name := c.Arguments[1]
	log.Printf("Adding device %s with name %s", device, name)

	db, err := openDatabase()
	if err != nil {
		log.Printf("Error opening database: %s", err)
		return DB_OPEN_ERROR
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		log.Printf("Error starting transaction: %s", err)
		return BEGIN_TRANSACTION_ERROR
	}
	defer tx.Commit()

	stmt, err := tx.Prepare("select name from devices where name = ?")
	if err != nil {
		log.Printf("Error preparing the statement: %s", err)
		return PREPARE_ERROR
	}
	defer stmt.Close()

	rows, err := stmt.Query(name)
	if err != nil {
		log.Printf("Error querying the device: %s", err)
		return QUERY_ERROR
	}
	defer rows.Close()

	deviceFound := false
	for rows.Next() {
		var device string
		err = rows.Scan(&device)
		if err != nil {
			log.Printf("Error scanning the row: %s", err)
			return SCAN_ERROR
		}
		deviceFound = true
	}

	if deviceFound {
		log.Printf("Device %s already exists", name)
		return DEVICE_EXISTS
	}

	out, err := exec.Command("blkid", "-s", "UUID", "-o", "value", device).Output()
	if err != nil {
		log.Printf("Error fetching device's UUID: %s", err)
		return DEVICE_UUID_ERROR
	}

	fs_uuid := strings.TrimSpace(string(out))

	stmt, err = tx.Prepare("select name from devices where fs_uuid = ?")
	if err != nil {
		log.Printf("Error preparing the statement: %s", err)
		return PREPARE_ERROR
	}
	defer stmt.Close()

	rows, err = stmt.Query(fs_uuid)
	if err != nil {
		log.Printf("Error querying the fs uuid: %s", err)
		return QUERY_ERROR
	}
	defer rows.Close()

	deviceFound = false
	var oldDevice string
	for rows.Next() {
		err = rows.Scan(&oldDevice)
		if err != nil {
			log.Printf("Error scanning the row: %s", err)
			return SCAN_ERROR
		}
		deviceFound = true
	}

	if deviceFound {
		log.Printf("Device with UUID %s already exists with name %s", fs_uuid, oldDevice)
		return DEVICE_EXISTS
	}

	stmt, err = tx.Prepare("insert into devices (name, fs_uuid) values (?, ?)")
	if err != nil {
		log.Printf("Error preparing the statement: %s", err)
		return PREPARE_ERROR
	}
	defer stmt.Close()

	_, err = stmt.Exec(name, fs_uuid)
	if err != nil {
		log.Printf("Error inserting the device: %s", err)
		return INSERT_ERROR
	}

	return SUCCESS
}

func (c *command) fileAdd() byte {
	device_name := c.Arguments[0]
	device_path := c.Arguments[1]
	local_path := c.Arguments[2]
	log.Printf("Mapping file %s from device %s to local file %s", device_path, device_name, local_path)

	return SUCCESS
}
