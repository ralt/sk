package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	server := cliServer{
		path: "/run/sk.cli.sock",
	}
	err := server.start()
	if err != nil {
		log.Printf("Failed to start the server: %v", err)
		os.Exit(1)
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGINT)
	<-signals
	server.stop()

	os.Exit(0)
}
