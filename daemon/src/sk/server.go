package main

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"log"
	"net"
	"os"
	"time"
	"syscall"
)

type cliServer struct {
	path     string
	stopped  bool
	stopChan chan int
}

func (s *cliServer) start() error {
	err := os.Remove(s.path)
	if err != nil {
		if pathError, ok := err.(*os.PathError); ok {
			if pathError.Err != syscall.ENOENT {
				return err
			}
		} else {
			return err
		}
	}

	listener, err := net.Listen("unix", s.path)
	if err != nil {
		return err
	}

	err = os.Chmod(s.path, os.FileMode(int(0777)))
	if err != nil {
		return err
	}

	server, ok := listener.(*net.UnixListener)
	if !ok {
		return errors.New("Unable to upgrade listener to UnixListener")
	}

	s.stopped = false
	s.stopChan = make(chan int)

	go s.acceptLoop(server)

	return nil
}

func (s *cliServer) acceptLoop(server *net.UnixListener) {
	var err error
	for {
		err = server.SetDeadline(time.Now().Add(time.Second))
		if err != nil {
			log.Printf("Listener deadline error: %v", err)
			return
		}

		fd, err := server.Accept()
		if err != nil {
			if err, ok := err.(*net.OpError); ok && err.Timeout() {
				if s.stopped {
					s.stopChan <- 1
					return
				}
				continue
			}
			log.Printf("Accept error: %v", err)
			continue
		}

		conn, ok := fd.(*net.UnixConn)
		if ok {
			// 1 minute ought to be enough for the client to do whatever it needs.
			if conn.SetDeadline(time.Now().Add(time.Minute)) != nil {
				log.Printf("Connection deadline error: %v", err)
				continue
			}
			go s.handle(conn)
		} else {
			log.Printf("Wrong socket?")
			continue
		}
	}
}

func (s *cliServer) handle(conn *net.UnixConn) {
	defer conn.Close()

	lengthBuffer := make([]byte, 4)
	readBytes, err := conn.Read(lengthBuffer)

	if err != nil {
		log.Printf("Error reading length: %v", err)
		return
	}

	if readBytes != 4 {
		log.Printf("Not enough bytes read for the length")
		return
	}

	length := binary.BigEndian.Uint32(lengthBuffer)

	commandBuffer := make([]byte, length)
	readBytes, err = conn.Read(commandBuffer)

	if err != nil {
		log.Printf("Error reading command: %v", err)
		return
	}

	if uint32(readBytes) != length {
		log.Printf("Not enough bytes read for the command")
		return
	}

	c := &command{}
	err = json.Unmarshal(commandBuffer, c)
	if err != nil {
		log.Printf("Unable to decode JSON")
		return
	}

	exitCode := make([]byte, 1)
	switch c.Name {
	case "device_add":
		exitCode[0] = c.deviceAdd()
	case "file_add":
		exitCode[0] = c.fileAdd()
	default:
		exitCode[0] = c.unknownCommand()
	}

	conn.Write(exitCode)
}

func (s *cliServer) stop() {
	s.stopped = true
	<-s.stopChan
}
